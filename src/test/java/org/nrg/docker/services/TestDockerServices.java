package org.nrg.docker.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.nrg.docker.entities.DockerServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@Rollback
@Transactional
public class TestDockerServices {
    @Test
    public void testSimpleDockerServer() {
        _log.debug("Starting the simple Docker server test.");
        final DockerServer server1 = _serverService.create("http://docker-xnat.nrg.mir", "2");
        assertEquals(1, _serverService.getCount());
        final DockerServer retrieved1 = _serverService.retrieve(server1.getId());
        assertEquals(retrieved1, server1);
        final DockerServer server2 = _serverService.create("http://docker-xnat.nrg.mir", 2376, "1");
        final DockerServer server3 = _serverService.create("http://xxx.nrg.mir", "1");
        final List<DockerServer> servers = _serverService.getAll();
        final List<DockerServer> serversByUrl = _serverService.findByUrl("http://docker-xnat.nrg.mir");
        final List<DockerServer> serversByVersion = _serverService.findByVersion("1");
        assertEquals(3, servers.size());
        assertEquals(2, serversByUrl.size());
        assertEquals(2, serversByVersion.size());
        assertTrue(serversByUrl.contains(server1));
        assertTrue(serversByUrl.contains(server2));
        assertTrue(serversByVersion.contains(server2));
        assertTrue(serversByVersion.contains(server3));
    }

    private static final Logger _log = LoggerFactory.getLogger(TestDockerServices.class);

    @Inject
    private DockerServerService _serverService;
}
