package org.nrg.docker.repositories;

import org.nrg.docker.entities.DockerServer;
import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.springframework.stereotype.Repository;

@Repository
public class DockerServerDAO extends AbstractHibernateDAO<DockerServer> {

}
