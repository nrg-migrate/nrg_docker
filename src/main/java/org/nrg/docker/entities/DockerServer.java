package org.nrg.docker.entities;

import org.apache.commons.lang3.StringUtils;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Entity;

@Entity
public class DockerServer extends AbstractHibernateEntity {
    public DockerServer(final String url) {
        if (_log.isDebugEnabled()) {
            _log.debug("Creating new Docker server entity with URL {} (no version or port specified)", url);
        }
        setUrl(url);
    }

    public DockerServer(final String url, final int port) {
        if (_log.isDebugEnabled()) {
            _log.debug("Creating new Docker server entity with URL {} (no version specified)", url + (port == 2375 ? "" : ":" + port));
        }
        setUrl(url);
        setPort(port);
    }

    public DockerServer(final String url, final String version) {
        if (_log.isDebugEnabled()) {
            _log.debug("Creating new Docker version {} server entity with URL {}", version, url);
        }
        setUrl(url);
        setVersion(version);
    }

    public DockerServer(final String url, final int port, final String version) {
        if (_log.isDebugEnabled()) {
            _log.debug("Creating new Docker version {} server entity with URL {}", version, url + (port == 2375 ? "" : ":" + port));
        }
        setUrl(url);
        setPort(port);
        setVersion(version);
    }

    public String getUrl() {
        return _url;
    }

    public void setUrl(final String url) {
        _url = url;
    }

    public int getPort() {
        return _port;
    }

    public void setPort(final int port) {
        _port = port;
    }

    public String getVersion() {
        return _version;
    }

    public void setVersion(final String version) {
        _version = version;
    }

    @Override
    public String toString() {
        return getUrl() + (getPort() == 2375 ? "" : ":" + getPort()) + (StringUtils.isBlank(getVersion()) ? "" : " (version " + getVersion() + ")");
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final DockerServer server = (DockerServer) o;

        return getPort() == server.getPort() && !(getUrl() != null ? !getUrl().equals(server.getUrl()) : server.getUrl() != null) && !(getVersion() != null ? !getVersion().equals(server.getVersion()) : server.getVersion() != null);

    }

    @Override
    public int hashCode() {
        int result = getUrl() != null ? getUrl().hashCode() : 0;
        result = 31 * result + getPort();
        result = 31 * result + (getVersion() != null ? getVersion().hashCode() : 0);
        return result;
    }

    private static final Logger _log = LoggerFactory.getLogger(DockerServer.class);

    private String _url;
    private int _port =  2375;
    private String _version;
}
