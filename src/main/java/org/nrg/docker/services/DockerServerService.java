package org.nrg.docker.services;

import org.nrg.docker.entities.DockerServer;
import org.nrg.framework.orm.hibernate.BaseHibernateService;

import java.util.List;

public interface DockerServerService extends BaseHibernateService<DockerServer> {
    List<DockerServer> findByUrl(final String url);
    List<DockerServer> findByVersion(final String version);
}
