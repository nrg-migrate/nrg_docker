package org.nrg.docker.services.impl.hibernate;

import org.nrg.docker.entities.DockerServer;
import org.nrg.docker.repositories.DockerServerDAO;
import org.nrg.docker.services.DockerServerService;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class HibernateDockerServerService extends AbstractHibernateEntityService<DockerServer, DockerServerDAO> implements DockerServerService {
    @Override
    @Transactional
    public List<DockerServer> findByUrl(final String url) {
        return getDao().findByExample(new DockerServer(url), EXCLUDED_PROPS_URL);
    }

    @Override
    @Transactional
    public List<DockerServer> findByVersion(final String version) {
        return getDao().findByExample(new DockerServer("", version), EXCLUDED_PROPS_VERSION);
    }

    private static final String[] EXCLUDED_PROPS_URL = AbstractHibernateEntity.getExcludedProperties("port", "version");
    private static final String[] EXCLUDED_PROPS_VERSION = AbstractHibernateEntity.getExcludedProperties("url", "port");
}
